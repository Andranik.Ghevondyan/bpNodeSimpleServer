const mongoose = require('mongoose')
const debug = require('debug')('my-project:db');

const {
    MONGO_URI,
} = require('../../constants')

async function connect() {
    try {
        const connection = await mongoose.connect(MONGO_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        })
        debug(`Successfully connected to mongodb at uri: ${MONGO_URI}`)
        return connection
    }catch(err) {
        debug(`Can NOT connect to mongodb at uri: ${MONGO_URI}`)
        throw err
    }
}

module.exports = connect