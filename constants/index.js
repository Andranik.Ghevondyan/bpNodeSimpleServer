function setConstants() {
    if(!process.env.MONGO_URI) throw new Error(errMsg('MONGO_URI'))
    
    return {
        MONGO_URI: process.env.MONGO_URI,

        // ENV CONSTANTS
        PORT: normalizePort(process.env.PORT || '3000'),
        NODE_ENV: normalizeEnv(process.env.NODE_ENV),
        DEBUG: process.env.DEBUG ? process.env.DEBUG : '',
    }
}

module.exports = setConstants()


function normalizePort(val) {
    const port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

function normalizeEnv(val) {
    const nodeEnv = ['development', 'production']
    return nodeEnv.includes(val) ? val : 'development'
}


function errMsg(ConstantName) {
    return `Please provide ${ConstantName} in .env`
}