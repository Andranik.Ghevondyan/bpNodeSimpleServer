let io;

module.exports = {
    create: (http) => {
        io = require('socket.io')(http, {
            cors: {
                origin: '*',
            }
        });
    },
    emit: (event, data) => {
        if (!io) {
            throw new Error('Not created!');
        }
        io.emit(event, data);
    }
};
