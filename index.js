const createError = require('http-errors');
const logger = require('morgan');

const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');


const connectToDb = require('./src/db')
const indexRouter = require('./src/routes/index');

const server = require('./server')


async function init() {
    // Include Your application initialization stuff here.
    await connectToDb()

    // view engine setup
    const app = express();

    app.options('*', cors()) // include before other routes

    app.use(cors({
        origin: true,
    }));
    app.use(logger('dev'));
    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));
    app.use(cookieParser());

    app.use('/', indexRouter);
    // Here you can Use your routes,
    // or create separate routes in "routes" folder and use that.

    // catch 404 and forward to error handler
    app.use(function (req, res, next) {
        next(createError(404));
    });

    // error handler
    app.use(function (err, req, res, next) {
        // set locals, only providing error in development
        const error = req.app.get('env') === 'development' ? err : {};
        // render the error page
        res.status(err.status || 500);
        res.send(error.message)
    });

    server(app)
}

init()